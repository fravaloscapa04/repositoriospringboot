package com.example.lab01.service;

import com.example.lab01.core.Customer;
import com.example.lab01.repository.CustomerRepository;
import com.example.lab01.repository.CustomerRepositoryImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImplV2 implements CustomerService {

    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImplV2.class);

    //     @Autowired
    CustomerRepository customerRepository;

    public CustomerServiceImplV2(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public String upper (String input){
        return input.toLowerCase()+customerRepository.getById(2);
    }

    @Override
    public List<Customer> getList() {
        return customerRepository.getList();
    }

    @Override
    public Customer getById(int id) {
        return customerRepository.getById(id);
    }

    @Override
    public void save(Customer customer) {
        log.debug("save");
        customerRepository.save(customer);
    }

    @Override
    public void update(Customer customer) {
        log.info("update {}",customer.getCustomerId());
        customerRepository.update(customer);
    }

    @Override
    public void delete(Customer customer) {
        log.info("delete {}",customer.getCustomerId());
        customerRepository.delete(customer);
    }

}
