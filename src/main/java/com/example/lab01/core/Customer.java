package com.example.lab01.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Customer {

    @JsonProperty("id")
    private int customerId;
    private String nombre;
    private String paternos;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaternos() {
        return paternos;
    }

    public void setPaternos(String paternos) {
        this.paternos = paternos;
    }
}
