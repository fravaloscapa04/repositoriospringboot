package com.example.lab01.service;

import com.example.lab01.core.Customer;

import java.util.List;

public interface CustomerService {

    String upper ( String input);

    List<Customer> getList();
    Customer getById(int id);

    void save(Customer customer);
    void update(Customer customer);
    void delete(Customer customer);
}
