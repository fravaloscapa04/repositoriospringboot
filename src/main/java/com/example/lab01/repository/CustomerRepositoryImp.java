package com.example.lab01.repository;

import com.example.lab01.core.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class CustomerRepositoryImp implements CustomerRepository{

private static final Logger log = LoggerFactory.getLogger(CustomerRepositoryImp.class);
    @Override
    public List<Customer> getList() {

        Customer customer = new Customer();
        customer.setCustomerId((1));
        customer.setNombre("Frank");

        Customer customer2 = new Customer();
        customer2.setCustomerId((2));
        customer2.setNombre("Jair del 8");

        List<Customer> lista= Arrays.asList(customer,customer2);

        return lista;
    }
    @Override
    public Customer getById(int id) {
        Customer customer = new Customer();
        if(id == 1) {
            customer.setCustomerId((id));
            customer.setNombre("Frank");
        }else if ( id == 2){
            customer.setCustomerId((2));
            customer.setNombre("Jair del 8");
        }
        return customer;
    }

    @Override
    public void save(Customer customer) {
        log.debug("save");
    }

    @Override
    public void update(Customer customer) {
        log.info("update {}",customer.getCustomerId());
    }

    @Override
    public void delete(Customer customer) {
        log.info("delete {}",customer.getCustomerId());
    }
}
