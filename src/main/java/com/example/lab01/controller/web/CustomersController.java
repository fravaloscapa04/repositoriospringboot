package com.example.lab01.controller.web;

import com.example.lab01.controller.web.dto.CustomerWebDto;
import com.example.lab01.core.Customer;
import com.example.lab01.service.CustomerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomersController {

   // @Autowired
    private CustomerService customerService;

    public CustomersController(CustomerService customerService){
        this.customerService=customerService;
    }

    @GetMapping
    public CustomerWebDto doThing(){

        CustomerWebDto customerWebDto = new CustomerWebDto();

        customerWebDto.setCustomers(customerService.getList());

        return customerWebDto;
    }

    @GetMapping("/{id}")
    public CustomerWebDto getById(@PathVariable("id") int id){
        CustomerWebDto customerWebDto = new CustomerWebDto();


        customerWebDto.setCustomer(customerService.getById(id));

        return customerWebDto;

    }

    @PostMapping
    public void create (@RequestBody Customer customer){
        StringUtils.capitalize(customer.getNombre());
        customerService.save(customer);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable(name="id") int id,
                       @RequestBody Customer customer){
        customer.setCustomerId(id);
        customerService.update(customer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name="id") int id){

        Customer customer= new Customer();
        customer.setCustomerId(id);

        customerService.delete(customer);
    }


}
