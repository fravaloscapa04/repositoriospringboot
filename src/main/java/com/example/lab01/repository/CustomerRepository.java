package com.example.lab01.repository;

import com.example.lab01.core.Customer;

import java.util.List;

public interface CustomerRepository {


    List<Customer> getList();

    Customer getById(int id);

    void save(Customer customer);
    void update(Customer customer);
    void delete(Customer customer);
}
